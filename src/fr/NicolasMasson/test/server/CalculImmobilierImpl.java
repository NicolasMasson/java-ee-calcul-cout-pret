package fr.NicolasMasson.test.server;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.NicolasMasson.test.client.ICalculImmobilierService;
import fr.NicolasMasson.test.shared.DataListsImmo;

public class CalculImmobilierImpl extends RemoteServiceServlet implements ICalculImmobilierService{

	@Override
	public DataListsImmo getDataTable(Double unMontant, Double unTaux,
			Double uneMensualite, Double uneDuree)
			throws IllegalArgumentException {
		DataListsImmo data = new DataListsImmo();
		ClasseOutil calcultor = new ClasseOutil(unMontant, unTaux, uneMensualite, uneDuree);
		data.setListAmortissement(calcultor.getAmortissementParAnnee());
		data.setListInterets(calcultor.getInteretParAnnee());
		data.setListResteDu(calcultor.getResteDuParAnnee());
		calcultor = null;

		return data;
		
		
	}

	@Override
	public Map<String, Double> getParameters(Double unMontant, Double unTaux,
			Double uneMensualite, Double uneDuree)
			throws IllegalArgumentException {
		ClasseOutil calcultor = new ClasseOutil(unMontant, unTaux, uneMensualite, uneDuree);
		Map<String, Double> parameters = new HashMap<String, Double>();
		parameters.put("montant", calcultor.getMontant());
		parameters.put("taux", calcultor.getTaux());
		parameters.put("mensualite", calcultor.getMensualite());
		parameters.put("duree", calcultor.getDuree());
		return parameters;
	}

}
