package fr.NicolasMasson.test.shared;

import java.io.Serializable;
import java.util.List;

public class DataListsImmo implements Serializable {
	private List<Double> listAmortissement, listInterets, listResteDu;

	public List<Double> getListAmortissement() {
		return listAmortissement;
	}

	public void setListAmortissement(List<Double> listAmortissement) {
		this.listAmortissement = listAmortissement;
	}

	public List<Double> getListInterets() {
		return listInterets;
	}

	public void setListInterets(List<Double> listInterets) {
		this.listInterets = listInterets;
	}

	public List<Double> getListResteDu() {
		return listResteDu;
	}

	public void setListResteDu(List<Double> listResteDu) {
		this.listResteDu = listResteDu;
	}
}
