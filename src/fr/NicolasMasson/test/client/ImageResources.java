package fr.NicolasMasson.test.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.resources.client.ImageResource.ImageOptions;

public interface ImageResources extends ClientBundle {
	 
    public static final ImageResources INSTANCE = GWT.create(ImageResources.class);
 
    @Source("resources/close.png")
    @ImageOptions(height = 18, width = 18)
    ImageResource close();
 
}
