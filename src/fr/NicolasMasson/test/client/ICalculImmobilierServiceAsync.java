package fr.NicolasMasson.test.client;

import java.util.Map;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.NicolasMasson.test.shared.DataListsImmo;

public interface ICalculImmobilierServiceAsync {

	void getDataTable(Double unMontant, Double unTaux, Double uneMensualite,
			Double uneDuree, AsyncCallback<DataListsImmo> callback);

	void getParameters(Double unMontant, Double unTaux, Double uneMensualite,
			Double uneDuree, AsyncCallback<Map<String, Double>> callback);

}
