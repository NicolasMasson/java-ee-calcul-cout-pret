package fr.NicolasMasson.test.client;

import java.util.Map;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.NicolasMasson.test.shared.DataListsImmo;

@RemoteServiceRelativePath("CalculImmobilier")
public interface ICalculImmobilierService extends RemoteService {
	DataListsImmo getDataTable(Double unMontant, Double unTaux, Double uneMensualite, Double uneDuree) throws IllegalArgumentException;
	Map<String, Double> getParameters(Double unMontant, Double unTaux, Double uneMensualite, Double uneDuree) throws IllegalArgumentException;
}
