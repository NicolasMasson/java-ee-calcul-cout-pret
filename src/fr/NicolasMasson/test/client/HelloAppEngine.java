package fr.NicolasMasson.test.client;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DecoratedTabPanel;
import com.google.gwt.user.client.ui.DecoratorPanel;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.visualization.client.AbstractDataTable.ColumnType;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.VisualizationUtils;
import com.google.gwt.visualization.client.visualizations.corechart.AreaChart;
import com.google.gwt.visualization.client.visualizations.corechart.AxisOptions;
import com.google.gwt.visualization.client.visualizations.corechart.CoreChart;
import com.google.gwt.visualization.client.visualizations.corechart.Options;

import fr.NicolasMasson.test.client.ihm.SummaryDecoratorPanel;
import fr.NicolasMasson.test.shared.DataListsImmo;


/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class HelloAppEngine implements EntryPoint {
	private TextBox txtMontant, txtTaux, txtMenusalite, txtDuree;
	private Button btnCalcul, btnClear;
	private VerticalPanel vPanelRoot;
	private DecoratedTabPanel tabPanel;
	private NumberFormat df = NumberFormat.getFormat("#.#");
	private NumberFormat df2 = NumberFormat.getFormat("#,##0.0#");
	private DialogBox msgBox;
	private Double unMontant = 0d, unTaux = 0d, uneMensualite= 0d, uneDuree = 0d;

	private final ICalculImmobilierServiceAsync calculImmobilierService = GWT.create(ICalculImmobilierService.class);

	public void onModuleLoad() {

		msgBox = new DialogBox();
		msgBox.setText("Veuillez n'enter que des chiffres dans les champs");
		msgBox.setTitle("Erreur de saisie");
		msgBox.setModal(true);
		Button btnOk = new Button();
		btnOk.setText("ok");
		msgBox.add(btnOk);

		btnOk.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				msgBox.hide();

			}
		});

		KeyUpHandler keyPressHandler = new KeyUpHandler() {

			@Override
			public void onKeyUp(KeyUpEvent event) {
				grayField();

			}


		};

		KeyDownHandler keyDownHandler = new KeyDownHandler() {

			@Override
			public void onKeyDown(KeyDownEvent event) {
				grayField();

			}
		};

		VerticalPanel vPanelLabels = new VerticalPanel();
		VerticalPanel vPanelTextBoxes = new VerticalPanel();
		VerticalPanel vPanelLabelsUnits = new VerticalPanel();


		Label lblMontant = new Label("Montant : ");
		txtMontant = new TextBox();
		txtMontant.addKeyUpHandler(keyPressHandler);
		txtMontant.addKeyDownHandler(keyDownHandler);
		Label lblEuros = new Label("Euros");

		vPanelLabels.add(lblMontant);
		vPanelTextBoxes.add(txtMontant);
		vPanelLabelsUnits.add(lblEuros);



		Label lblTaux = new Label("Taux Annuel : ");
		txtTaux = new TextBox();
		Label lblPourcentage = new Label("%");

		vPanelLabels.add(lblTaux);
		vPanelTextBoxes.add(txtTaux);
		vPanelLabelsUnits.add(lblPourcentage);

		Label lblMensualite = new Label("Menusalités : ");
		txtMenusalite = new TextBox();
		txtMenusalite.addKeyUpHandler(keyPressHandler);
		txtMenusalite.addKeyDownHandler(keyDownHandler);
		Label lblEuros2 = new Label("Euros");


		vPanelLabels.add(lblMensualite);
		vPanelTextBoxes.add(txtMenusalite);
		vPanelLabelsUnits.add(lblEuros2);


		Label lblDuree = new Label("Durée : ");
		txtDuree = new TextBox();
		txtDuree.addKeyUpHandler(keyPressHandler);
		txtDuree.addKeyDownHandler(keyDownHandler);
		Label lblAnnees = new Label("Années");


		vPanelLabels.add(lblDuree);
		vPanelTextBoxes.add(txtDuree);
		vPanelLabelsUnits.add(lblAnnees);



		btnCalcul = new Button();
		btnCalcul.setText("Calcul");
		btnCalcul.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if (verifyFields() == true)
				{

					if (!"".equals(txtMontant.getText())){
						unMontant = Double.valueOf(txtMontant.getText());
					}else {
						unMontant = 0d;
					}
					if (!"".equals(txtTaux.getText())){
						unTaux = Double.valueOf(txtTaux.getText());
					}else {
						unTaux = 0d;
					}
					if (!"".equals(txtMenusalite.getText())){
						uneMensualite = Double.valueOf(txtMenusalite.getText());
					}else {
						uneMensualite = 0d;
					}
					if (!"".equals(txtDuree.getText())){
						uneDuree = (double)Math.round(Double.valueOf(txtDuree.getText()) * 1) / 1;
						txtDuree.setText(String.valueOf(uneDuree));
					}else {
						uneDuree = 0d;
					}


					calculImmobilierService.getParameters(unMontant, unTaux, uneMensualite, uneDuree, new AsyncCallback<Map<String,Double>>() {

						@Override
						public void onFailure(Throwable caught) {


						}

						@Override
						public void onSuccess(Map<String, Double> result) {
							final SummaryDecoratorPanel summary = new SummaryDecoratorPanel();

							for (Iterator i = result.keySet().iterator() ; i.hasNext() ; ){
								Object key = i.next();

								if ("montant".equals(String.valueOf(key)))
								{
									summary.setValMontant(String.valueOf(df2.format(result.get(key)).replace(",", " ")));
									unMontant = result.get(key);
								}

								if ("taux".equals(String.valueOf(key)))
								{
									summary.setValTaux(String.valueOf(df2.format(result.get(key)).replace(",", " ")));
									unTaux = result.get(key);
								}

								if ("mensualite".equals(String.valueOf(key)))
								{
									summary.setValMensualite(String.valueOf(df2.format(result.get(key)).replace(",", " ")));
									uneMensualite = result.get(key);
								}

								if ("duree".equals(String.valueOf(key)))
								{
									summary.setValDuree(String.valueOf(df2.format(result.get(key)).replace(",", " ")));
									uneDuree = result.get(key);
								}
							}

							if (uneDuree == 99){
								msgBox.setText("Montant de mensualité inférieur aux intérets du par mois, impossible de faire le calcul");
								msgBox.center();
							}
							else
							{


								calculImmobilierService.getDataTable(unMontant, unTaux, uneMensualite, uneDuree, new AsyncCallback<DataListsImmo>() {

									@Override
									public void onSuccess(DataListsImmo result) {
										final List<Double> listAmortissement, listInterets, listResteDu;
										listAmortissement = result.getListAmortissement();
										listInterets = result.getListInterets();
										listResteDu = result.getListResteDu();
										VisualizationUtils.loadVisualizationApi(
												new Runnable() {
													public void run() {

														DataTable data = DataTable.create();
														data.addColumn(ColumnType.NUMBER, "Année");
														data.addColumn(ColumnType.NUMBER, "Capital Amorti");
														data.addColumn(ColumnType.NUMBER, "Interets");
														data.addRow();
														for (int i =0; i < listAmortissement.size(); i++)
														{
															data.addRow();
															data.setValue(i, 0, i+1);
															data.setValue(i, 1, Double.valueOf(df.format(listAmortissement.get(i))));
															data.setValue(i, 2, Double.valueOf(df.format(listInterets.get(i))));
														}

														Options chartOptions = Options.create();								
														AxisOptions hAxisOptions = AxisOptions.create();
														hAxisOptions.setTitle("Années");
														hAxisOptions.setMinValue(0d);
														hAxisOptions.setMaxValue(Double.valueOf(listAmortissement.size() + 1));
														chartOptions.setHAxisOptions(hAxisOptions);
														HorizontalPanel hPanelInstance = new HorizontalPanel();
														AreaChart chartAmortissement = new AreaChart(data, chartOptions);

														Double totalInteret = 0d, totalAmorti = 0d;

														for (int i = 0; i < listAmortissement.size(); i++)
														{
															totalInteret = totalInteret + listInterets.get(i);
															totalAmorti = totalAmorti + listAmortissement.get(i);
														}

														totalAmorti = Double.valueOf(df.format(totalAmorti));
														totalInteret = Double.valueOf(df.format(totalInteret));
														summary.addTotaux(totalInteret, totalAmorti);
														DecoratorPanel decPanel = summary.getDecoratorPanel();
														decPanel.setSize("300px", "200px");
														decPanel.setVisible(true);

														chartAmortissement.setTitle("Comparaison capital amorti / interet par années");

														DataTable dataInteretCumulatif = DataTable.create();
														dataInteretCumulatif.addColumn(ColumnType.NUMBER, "Année");
														dataInteretCumulatif.addColumn(ColumnType.NUMBER, "Cumulatif capital amorti");
														dataInteretCumulatif.addColumn(ColumnType.NUMBER, "Cumulatif intérets");
														totalInteret = 0d;
														totalAmorti = 0d;
														dataInteretCumulatif.addRow();
														dataInteretCumulatif.setValue(0, 0, 0);
														dataInteretCumulatif.setValue(0, 1, 0);
														dataInteretCumulatif.setValue(0, 2, 0);
														for (int i =0; i < listInterets.size(); i++)
														{
															dataInteretCumulatif.addRow();
															dataInteretCumulatif.setValue(i+1, 0, i+1);
															totalInteret = totalInteret + Double.valueOf(df.format(listInterets.get(i)));
															totalAmorti = totalAmorti + Double.valueOf(df.format(listAmortissement.get(i)));
															dataInteretCumulatif.setValue(i+1, 1, Double.valueOf(df.format(totalAmorti)));
															dataInteretCumulatif.setValue(i+1, 2, Double.valueOf(df.format(totalInteret)));

														}

														CoreChart charInteretCumulatif = new CoreChart(dataInteretCumulatif, chartOptions) {};
														charInteretCumulatif.setTitle("Comparaison somme cumulative des intérets et capital amorti par années");
														charInteretCumulatif.setSize("100%", "300px");
														chartAmortissement.setSize("100%", "300px");
														hPanelInstance.add(decPanel);
														hPanelInstance.add(chartAmortissement);
														hPanelInstance.add(charInteretCumulatif);

														loadTab(hPanelInstance, String.valueOf(df.format(unMontant))
																+"/"+String.valueOf(df.format(unTaux))
																+"/"+String.valueOf(df.format(uneMensualite))
																+"/"+String.valueOf(df.format(uneDuree)));
														tabPanel.selectTab(tabPanel.getWidgetCount() - 1);

													}

												},CoreChart.PACKAGE
												);


									}

									@Override
									public void onFailure(Throwable caught) {
										// TODO Auto-generated method stub

									}
								});
							}
						}
					});

				}

			}
		});


		btnClear = new Button();
		btnClear.setText("Clear");
		btnClear.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				txtMontant.setText("");
				txtTaux.setText("");
				txtMenusalite.setText("");
				txtDuree.setText("");

				txtMontant.setEnabled(true);
				txtTaux.setEnabled(true);
				txtMenusalite.setEnabled(true);
				txtDuree.setEnabled(true);

			}
		});

		HorizontalPanel hPanelButtons = new HorizontalPanel();
		hPanelButtons.add(btnClear);
		hPanelButtons.add(btnCalcul);


		vPanelLabels.setCellHeight(lblMontant, "30px");
		vPanelLabels.setCellVerticalAlignment(lblMontant, HasVerticalAlignment.ALIGN_MIDDLE);
		vPanelLabels.setCellHeight(lblTaux, "30px");
		vPanelLabels.setCellVerticalAlignment(lblTaux, HasVerticalAlignment.ALIGN_MIDDLE);
		vPanelLabels.setCellHeight(lblMensualite, "30px");
		vPanelLabels.setCellVerticalAlignment(lblMensualite, HasVerticalAlignment.ALIGN_MIDDLE);
		vPanelLabels.setCellHeight(lblDuree, "30px");
		vPanelLabels.setCellVerticalAlignment(lblDuree, HasVerticalAlignment.ALIGN_MIDDLE);


		vPanelTextBoxes.setCellHeight(txtMontant, "30px");
		vPanelTextBoxes.setCellVerticalAlignment(txtMontant, HasVerticalAlignment.ALIGN_MIDDLE);
		vPanelTextBoxes.setCellHeight(txtTaux, "30px");
		vPanelTextBoxes.setCellVerticalAlignment(txtTaux, HasVerticalAlignment.ALIGN_MIDDLE);
		vPanelTextBoxes.setCellHeight(txtMenusalite, "30px");
		vPanelTextBoxes.setCellVerticalAlignment(txtMenusalite, HasVerticalAlignment.ALIGN_MIDDLE);
		vPanelTextBoxes.setCellHeight(txtDuree, "30px");
		vPanelTextBoxes.setCellVerticalAlignment(txtDuree, HasVerticalAlignment.ALIGN_MIDDLE);


		vPanelLabelsUnits.setCellHeight(lblEuros, "30px");
		vPanelLabelsUnits.setCellVerticalAlignment(lblEuros, HasVerticalAlignment.ALIGN_MIDDLE);
		vPanelLabelsUnits.setCellHeight(lblPourcentage, "30px");
		vPanelLabelsUnits.setCellVerticalAlignment(lblPourcentage, HasVerticalAlignment.ALIGN_MIDDLE);
		vPanelLabelsUnits.setCellHeight(lblEuros2, "30px");
		vPanelLabelsUnits.setCellVerticalAlignment(lblEuros2, HasVerticalAlignment.ALIGN_MIDDLE);
		vPanelLabelsUnits.setCellHeight(lblAnnees, "30px");
		vPanelLabelsUnits.setCellVerticalAlignment(lblAnnees, HasVerticalAlignment.ALIGN_MIDDLE);



		vPanelLabels.setSpacing(5);
		vPanelTextBoxes.setSpacing(5);
		vPanelLabelsUnits.setSpacing(5);
		hPanelButtons.setSpacing(30);

		HorizontalPanel hPanelMain = new HorizontalPanel();
		hPanelMain.add(vPanelLabels);
		hPanelMain.add(vPanelTextBoxes);
		hPanelMain.add(vPanelLabelsUnits);

		VerticalPanel vPanelMain = new VerticalPanel();
		vPanelMain.add(hPanelMain);
		vPanelMain.add(hPanelButtons);

		DecoratorPanel decPanel = new DecoratorPanel();
		decPanel.add(vPanelMain);
		
		

		vPanelRoot = new VerticalPanel();
		vPanelRoot.add(decPanel);

		tabPanel = new DecoratedTabPanel();
		//	    tabPanel.setAnimationDuration(1000);

		HorizontalPanel hTabHeaderCalcultCout = new HorizontalPanel();
		Label headerCalcultCout = new Label("Calcul cout pret");
		Label closeButtonCalculCout = new Label();
		closeButtonCalculCout.setText("x");

		hTabHeaderCalcultCout.add(headerCalcultCout);
		hTabHeaderCalcultCout.add(closeButtonCalculCout);
		hTabHeaderCalcultCout.setCellVerticalAlignment(headerCalcultCout, HasVerticalAlignment.ALIGN_BOTTOM);
		hTabHeaderCalcultCout.setCellVerticalAlignment(closeButtonCalculCout, HasVerticalAlignment.ALIGN_BOTTOM);
		hTabHeaderCalcultCout.setVerticalAlignment(HasVerticalAlignment.ALIGN_BOTTOM);
		tabPanel.add(vPanelRoot,"Paramètres");

		HTML aboutText = new HTML("<h1>Calcul de cout de pret </h1><br>" +
				"Cette application vous permet de visualiser (sous forme de graphiques) le cout d'un crédit à taux fixe (exemple : crédit immobilier) en fonction des paramètres que vous avez renseignés<br><br>" +
				"Vous pourrez ainsi voir : <br>" +
				"- Le cout total des intérets et le capital amorti sur toute la durée du crédit<br>" +
				"- Combien vont être vos intéret chaque année ainsi le capital amorti annuel<br>" +
				"- La somme cumulative des interets et du capital amorti en fonction du temps<br>" +
				"<br><br>" +
				"Explication des paramètres :<br>" +
				"- Le champs <b>Montant</b> correspond à <b>la somme que vous souhaitez emprunter en euros</b><br>" +
				"- Le champs <b>Taux</b> correspond au <b>taux d'intéret annuel (hors assurance)</b><br>" +
				"- Le champs <b>Mensualité</b> correspond au <b>montant de vous allez payer chaque mois pour le crédit</b><br>"+
				"- Le champs <b>Durée</b> correspond au <b>nombre d'années durant lesquel vous allez payer le crédit</b><br><br>"+
				"Notes :<br><br>"+
				"- Le champs durée est arrondi a l'entier supérieur ou inférieur si vous saisissez un nombre décimal<br>"+
				"- Pour les autres paramètres les nombre sont tronqués à la deuxième décimale<br>" +
				"<br>Nicolas Masson.<br>http://nicolas-masson.fr ");
		tabPanel.add(aboutText, "Aide");

		tabPanel.selectTab(0);

		tabPanel.setSize("100%", "800px");
		tabPanel.setAnimationEnabled(true);

		RootPanel.get().add(tabPanel);

	}

	private void loadTab(final Widget widget, String headingText) {

		HorizontalPanel panel = new HorizontalPanel();
		panel.setStyleName("tabHeader");
		Label text = new Label();
		text.setText(headingText);
		text.setStyleDependentName("text", true);
		//        Label close = new Label();
		ImageAnchor close = new ImageAnchor();
		//        close.setText("x");
		//        close.setTitle("Fermer la fenêtre");
		//        close.setStyleName("closeLabel");
		close.setResource(ImageResources.INSTANCE.close());
		text.setStyleDependentName("close", true);
		close.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				tabPanel.remove(tabPanel.getWidgetIndex(widget));
				tabPanel.selectTab(tabPanel.getWidgetCount() - 1);
			}
		});
		panel.add(text);
		panel.add(new HTML("&nbsp&nbsp&nbsp"));
		panel.add(close);
		panel.setCellHorizontalAlignment(text, HasHorizontalAlignment.ALIGN_LEFT);
		panel.setCellVerticalAlignment(text, HasVerticalAlignment.ALIGN_BOTTOM);
		panel.setCellHorizontalAlignment(close, HasHorizontalAlignment.ALIGN_RIGHT);
		panel.setCellVerticalAlignment(close, HasVerticalAlignment.ALIGN_BOTTOM);

		tabPanel.add(widget, panel);
	}

	private Boolean verifyFields(){
		int i = 0;
		Boolean boolErrorFound = false;
		try {

			if (!"".equals(txtMontant.getText())){
				i++;
				Double.valueOf(txtMontant.getText());				
			}

			if (!"".equals(txtMenusalite.getText())){
				i++;		
				Double.valueOf(txtMenusalite.getText());
			}

			if (!"".equals(txtDuree.getText())){
				i++;
				Double duree = Double.valueOf(txtDuree.getText());
				if (duree >= 99){
					msgBox.setText("Valeure trop grand dans le champs durée");
					msgBox.center();
					boolErrorFound = true;
				}
			}

			if (!"".equals(txtTaux.getText())){
				Double.valueOf(txtTaux.getText());
			}
		}catch (java.lang.NumberFormatException e){
			msgBox.setText("Veuillez n'enter que des chiffres dans les champs");
			msgBox.center();
			boolErrorFound = true;
		}

		if ("".equals(txtTaux.getText()) && (boolErrorFound == false)){
			msgBox.setText("Le champs taux d'intéret est obligatoire");
			msgBox.center();
			boolErrorFound = true;
		} 


		if ((i < 2)  && (boolErrorFound == false)){
			msgBox.setText("Au moins 3 champs doivent être rempli pour pouvoir lancer le calcul, le champs taux étant obligatoire");
			msgBox.center();
			boolErrorFound = true;
		}


		return !boolErrorFound;
	}

	private void grayField(){
		int i = 0;
		TextBox txtAGriser = null;
		txtMontant.setEnabled(true);
		txtMenusalite.setEnabled(true);
		txtDuree.setEnabled(true);
		if (!"".equals(txtMontant.getText())){
			i++;
		} else {
			txtAGriser = txtMontant;
		}

		if (!"".equals(txtMenusalite.getText())){
			i++;		
		} else {
			txtAGriser = txtMenusalite;
		}

		if (!"".equals(txtDuree.getText())){
			i++;		
		} else {
			txtAGriser = txtDuree;
		}

		if (i == 2){
			txtAGriser.setEnabled(false);
		}



	}
}



