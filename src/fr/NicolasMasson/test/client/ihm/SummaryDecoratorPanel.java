package fr.NicolasMasson.test.client.ihm;

import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.ui.DecoratorPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

public class SummaryDecoratorPanel {
	private VerticalPanel vPanelSummaryLabel, vPanelSummaryValues, vPanelSummaryUnits;
	private Label valMontant, valTaux, valMensualite, valDuree;
	private DecoratorPanel decPanelSummary;
	private NumberFormat df = NumberFormat.getFormat("#,##0.0#");
	
	public String getValMontant() {
		return valMontant.getText();
	}

	public void setValMontant(String valMontant) {
		this.valMontant.setText(valMontant);
	}

	public String getValTaux() {
		return valTaux.getText();
	}

	public void setValTaux(String valTaux) {
		this.valTaux.setText(valTaux);
	}

	public String getValMensualite() {
		return valMensualite.getText();
	}

	public void setValMensualite(String valMensualite) {
		this.valMensualite.setText(valMensualite);
	}

	public String getValDuree() {
		return valDuree.getText();
	}

	public void setValDuree(String valDuree) {
		this.valDuree.setText(valDuree);
	}

	public SummaryDecoratorPanel(){
		decPanelSummary = new DecoratorPanel();
		
		HorizontalPanel hPanelSummaryMain = new HorizontalPanel();
		
		vPanelSummaryLabel = new VerticalPanel();
		vPanelSummaryLabel.add(new Label("Montant"));
		vPanelSummaryLabel.add(new Label("Taux"));
		vPanelSummaryLabel.add(new Label("Mensualite"));
		vPanelSummaryLabel.add(new Label("Durée"));
		
		hPanelSummaryMain.add(vPanelSummaryLabel);
		
		vPanelSummaryValues = new VerticalPanel();
		valMontant = new Label("0");
		vPanelSummaryValues.add(valMontant);

		valTaux = new Label("0");
		vPanelSummaryValues.add(valTaux);
		
		valMensualite = new Label("0");
		vPanelSummaryValues.add(valMensualite);
		
		valDuree = new Label("0");
		vPanelSummaryValues.add(valDuree);
		
		vPanelSummaryValues.setCellHorizontalAlignment(valMontant, HasHorizontalAlignment.ALIGN_RIGHT);
		vPanelSummaryValues.setCellHorizontalAlignment(valTaux, HasHorizontalAlignment.ALIGN_RIGHT);
		vPanelSummaryValues.setCellHorizontalAlignment(valMensualite, HasHorizontalAlignment.ALIGN_RIGHT);
		vPanelSummaryValues.setCellHorizontalAlignment(valDuree, HasHorizontalAlignment.ALIGN_RIGHT);
		
		
		
		hPanelSummaryMain.add(vPanelSummaryValues);
		decPanelSummary.add(hPanelSummaryMain);
		Label lblEuros = new Label("Euros");
		Label lblPourcentage = new Label("%");
		Label lblEuros2 = new Label("Euros");
		Label lblAnnées = new Label("Années");
		
		vPanelSummaryUnits = new VerticalPanel();
		vPanelSummaryUnits.add(lblEuros);
		vPanelSummaryUnits.add(lblPourcentage);
		vPanelSummaryUnits.add(lblEuros2);
		vPanelSummaryUnits.add(lblAnnées);
		
		vPanelSummaryUnits.setCellHorizontalAlignment(lblEuros, HasHorizontalAlignment.ALIGN_LEFT);
		vPanelSummaryUnits.setCellHorizontalAlignment(lblPourcentage, HasHorizontalAlignment.ALIGN_LEFT);
		vPanelSummaryUnits.setCellHorizontalAlignment(lblEuros2, HasHorizontalAlignment.ALIGN_LEFT);
		vPanelSummaryUnits.setCellHorizontalAlignment(lblAnnées, HasHorizontalAlignment.ALIGN_LEFT);
		
		hPanelSummaryMain.add(vPanelSummaryUnits);
		
		
		
		
		vPanelSummaryLabel.setSpacing(5);
		vPanelSummaryValues.setSpacing(5);
		vPanelSummaryUnits.setSpacing(5);
	}
	
	public void addTotaux (Double totalInterets, Double totalAmorti)
	{
		
		vPanelSummaryLabel.add(new HTML("&nbsp"));
		vPanelSummaryLabel.add(new HTML("&nbsp"));
		vPanelSummaryValues.add(new HTML("&nbsp"));
		vPanelSummaryValues.add(new HTML("&nbsp"));
		vPanelSummaryUnits.add(new HTML("&nbsp"));
		vPanelSummaryUnits.add(new HTML("&nbsp"));
		
		vPanelSummaryLabel.add(new Label("Cout total du crédit :"));
		vPanelSummaryLabel.add(new Label("Total amorti : "));
		
		Label valTotalAmorti = new Label(df.format(totalInterets).replace(",", " "));
		Label valTotalInterets = new Label(df.format(totalAmorti).replace(",", " "));
		vPanelSummaryValues.add(valTotalAmorti);
		vPanelSummaryValues.add(valTotalInterets);
		vPanelSummaryValues.setCellHorizontalAlignment(valTotalAmorti, HasHorizontalAlignment.ALIGN_RIGHT);
		vPanelSummaryValues.setCellHorizontalAlignment(valTotalInterets, HasHorizontalAlignment.ALIGN_RIGHT);
		
		Label lblEuros = new Label("Euros");
		Label lblEuros2 = new Label("Euros");
		vPanelSummaryUnits.add(lblEuros);
		vPanelSummaryUnits.add(lblEuros2);
		vPanelSummaryUnits.setCellHorizontalAlignment(lblEuros, HasHorizontalAlignment.ALIGN_LEFT);
		vPanelSummaryUnits.setCellHorizontalAlignment(lblEuros2, HasHorizontalAlignment.ALIGN_LEFT);
	
	}
	
	public DecoratorPanel getDecoratorPanel(){
		return decPanelSummary;
	}
	
}
